;; Set dired-use-ls-dired to nil to avoid ls-dired error
(setq dired-use-ls-dired nil)

;; Add moe-theme
(use-package moe-theme :ensure t)
;; initialize moe-dark
(moe-dark)


;; Add which-key
(use-package which-key :ensure t)
;; initialize which-key-mode
(which-key-mode)

;; maximize to full screen
(add-to-list 'default-frame-alist '(fullscreen . maximized))

;; bind next-buffer to M-s-right
;; Bindx pervious-buffer to M-s-left

(global-set-key (kbd "<M-s-left>") 'previous-buffer)
(global-set-key (kbd "<M-s-right>") 'next-buffer)


;; Git status
(global-set-key (kbd "C-c g") 'magit-status)

;; Remove trailing whitespace before saving
(add-hook 'before-save-hook 'delete-trailing-whitespace)

;; Highlight current line
(global-hl-line-mode)

;; Projectile
(projectile-mode t)
(global-set-key (kbd "C-c p f") 'projectile-find-file)

(global-set-key (kbd "C-c j i") 'cider-jack-in)

(setq make-backup-files nil) ; stop creating backup~ files
(setq auto-save-default nil) ; stop creating #autosave# files
